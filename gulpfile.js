var elixir = require('laravel-elixir');
require('laravel-elixir-sass-compass');

elixir(function(mix) {

	mix.compass('frontend.scss', 'public/css', {
		modules: ['susy'],
		config_file: "config.rb",
		style: "expanded",
		sass: "resources/assets/sass",
		font: "public/fonts",
		image: "public/images",
		javascript: "public/js",
		sourcemap: true
	});

	mix.compass('admin.scss', 'public/css', {
		modules: ['susy'],
		config_file: "config.rb",
		style: "expanded",
		sass: "resources/assets/sass",
		font: "public/fonts",
		image: "public/images",
		javascript: "public/js",
		sourcemap: true
	});

	mix.scriptsIn('resources/assets/js/frontend*', 'public/js/frontend.js');
	mix.scriptsIn('resources/assets/js/admin*', 'public/js/admin.js');
});
