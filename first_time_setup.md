1) Setup your homestead base environment (if that's where you're testing)
- Setup folder name in `Homestead.yaml`
- Setup host name in `hosts` file
- Create database for your application (if relevant)

2) Copy `.env.example` to `.env`

3) Configure .env with homestead database (if relevant)

4) Setup app namespace with `php artisan app:name`
After that, clear the precompiled files with `composer dump-autoload` and `php artisan optimize`
Don't forget to regenerate your IDE Helper with `php artisan ide-helper:generate`

5) Run the base auth migrations by calling `php artisan migrate` (if relevant)

6) If you're having admin users or user authentication, might be a good idea to register the developer user.
Do it by calling `php artisan user:register`

7) Update `composer.json` with your project details

8) Update `readme.md` with project name and short description

9) Do initial git setup
- Run `git init`
- Run `git add -A .`
- Run `git commit -m "Initial commit"`
- Create repository in BitBucket
- Run `git remote add origin <BITBUCKET_URL>`
- Run `git push origin master`

10) Start writing your application!
- Draw database schema
- Write base table migrations
- Write base Models
- Write barebone controllers
- Write route definitions in `routes.php`