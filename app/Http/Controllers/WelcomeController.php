<?php
/**
 * WelcomeController.php
 * 
 * Copyright (c) LQDI Digital
 * www.lqdi.net - 2015
 * 
 * @author Aryel Tupinambá <aryel.tupinamba@lqdi.net>
 * 
 * Created at: 4/23/15, 17:48
 */

namespace App\Http\Controllers;

use Exception;

class WelcomeController extends Controller {

	public function __construct() {

	}

	public function index() {
		return view('frontend.welcome');
	}

	public function test_exception() {
		throw new Exception("Hello, error!");
	}

}