<?php
/**
 * DashboardController.php
 * 
 * Copyright (c) LQDI Digital
 * www.lqdi.net - 2015
 * 
 * @author Aryel Tupinambá <aryel.tupinamba@lqdi.net>
 * 
 * Created at: 4/27/15, 12:54
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

	public function __construct() {
		$this->middleware("auth");
	}

	public function index() {
		return view("admin.dashboard");
	}

}