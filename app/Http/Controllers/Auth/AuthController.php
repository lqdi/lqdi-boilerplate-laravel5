<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Exception;
use Redirect;
use Request;
use Response;
use App\Http\Controllers\Controller;
use Validator;


class AuthController extends Controller {

	/**
	 * Called when the user wishes to login
	 * @return \Illuminate\View\View
	 */
	public function login() {
		return view('auth.login');
	}

	/**
	 * Called when the user attempts to logout
	 */
	public function logout() {
		if(!Auth::guest()) {
			Auth::logout();
		}

		return $this->onAuthLogout();
	}

	/**
	 * Called when the user attempts to authenticate
	 */
	public function authenticate() {
		try {

			$email = Request::get('email');
			$password = Request::get('password');

			if (Auth::attempt(['email' => $email, 'password' => $password])) {
				return $this->onAuthSuccess();
			}

			return $this->onAuthFail();

		} catch (Exception $ex) {
			return $this->onAuthError($ex);
		}
	}

	/**
	 * Called when the user logs in succesfully
	 * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	private function onAuthSuccess() {
		if(Request::ajax()) {
			return Response::json(['status' => 'AUTH_OK', 'userID' => Auth::getUser()->getAuthIdentifier()]);
		}

		return Redirect::route("admin.dashboard");
	}

	/**
	 * Called when the given credentials are incorrect
	 * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	private function onAuthFail() {
		if(Request::ajax()) {
			return response()->json(['status' => 'AUTH_FAIL']);
		}

		return redirect()->route('auth.login', ['status' => 'failed']);
	}

	/**
	 * Called when an exception is raised during login
	 * @param Exception $ex
	 * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	private function onAuthError(Exception $ex) {
		if(Request::ajax()) {
			return response()->json(['status' => 'AUTH_ERROR', 'code' => $ex->getCode(), 'message' => $ex->getMessage()]);
		}

		return redirect()->route('auth.login', ['status' => 'error', 'errorCode' => $ex->getCode()]);
	}

	/**
	 * Called after the user logs out
	 * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	private function onAuthLogout() {
		if(Request::ajax()) {
			return response()->json(['status' => 'LOGGED_OUT']);
		}

		return redirect()->route('auth.login', ['status' => 'logged_out']);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data) {

		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);

	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data) {

		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

	}

}
