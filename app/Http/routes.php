<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {
	Route::get('login', ['as' => 'auth.login', 'uses' => 'AuthController@login']);
	Route::get('logout', ['as' => 'auth.logout', 'uses' => 'AuthController@logout']);
	Route::post('authenticate', ['as' => 'auth.authenticate', 'uses' => 'AuthController@authenticate']);
});

Route::controller('password', 'Auth\PasswordController');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
	Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);
});



Route::get('/', ['as' => 'welcome', 'uses' => 'WelcomeController@index']);
Route::get('/exception', ['as' => 'welcome.exception', 'uses' => 'WelcomeController@test_exception']);

