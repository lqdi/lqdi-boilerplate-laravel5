<?php
/**
 * L5 Boilerplate
 * RegisterUser.php
 * 
 * Copyright (c) LQDI Digital
 * www.lqdi.net - 2015
 * 
 * @author Aryel Tupinambá <aryel.tupinamba@lqdi.net>
 * 
 * Created at: 4/27/15, 12:48
 */

namespace App\Console\Commands;


use App\User;
use Illuminate\Console\Command;

class RegisterUser extends Command {

	protected $name = 'user:register';
	protected $description = 'Registers a user';

	public function handle() {

		$name = $this->ask("What is the user full name?");
		$email = $this->ask("What is the user e-mail?");
		$password = $this->ask("What is the user password?");

		$user = User::register($name, $email, $password);

		$this->info("User registered successfully! User ID: {$user->getAuthIdentifier()}");
	}

}