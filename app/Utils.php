<?php
/**
 * Utils.php
 *
 * Copyright (c) LQDI Digital
 * www.lqdi.net - 2015
 *
 * @author Aryel Tupinambá <aryel.tupinamba@lqdi.net>
 *
 * Created at: 25/09/15, 18:40
 */

namespace App;

use App;
use Intervention\Image\ImageManagerStatic as Image;

class Utils {

	public static function onFirst($num = 0, $ifYes = null, $ifNo = '') {
		static $flags = [];
		if(!isset($flags[$num])) {
			$flags[$num] = true;
			return ($ifYes)?$ifYes:true;
		}

		return ($ifYes)?$ifNo:false;
	}

	public static function counter($num = 0, $start = 0, $offset = 1) {
		static $counters = [];

		if(!isset($counters[$num])) {
			$counters[$num] = $start;
			return $counters[$num];
		}

		$counters[$num] += $offset;

		return $counters[$num];
	}

	public static function evenOdd($num = 0, $ifEven = 'even', $ifOdd = 'odd') {
		static $evodd = [];

		if(!isset($evodd[$num])) {
			$evodd[$num] = 0;
			return $ifEven;
		}

		$evodd[$num]++;

		if(($evodd[$num] % 2) == 0) {
			return $ifEven;
		}

		return $ifOdd;
	}

	public static function getSizeAspectRatio($width, $height) {
		if($width > $height) {
			return "horizontal";
		}

		if($height > $width) {
			return "vertical";
		}

		return "square";
	}

	public static function clearPhoneNumber($phone) {
		return str_replace(['(',')',' ', '.', '-',','], '', $phone);
	}

	public static function clearUserHTML($in) {
		return strip_tags($in, '<p><b><u><i><a><img><center><span><div><strong><small><big><ul><li>');
	}

	public static function clearUserStyles($in) {
		return preg_replace('/(<[^>]*) style=("[^"]+"|\'[^\']+\')([^>]*>)/i', '$1$3', $in);
	}

	public static function clearSEODescription($in) {
		$out = strip_tags($in);
		$out = str_replace(["\r","\n","\t"], " ", $out);
		$out = str_replace("\"", "\\\"", $out);
		return $out;
	}

	public static function extractYouTubeID($url) {
		if(!$url) return false;

		$queryString = substr($url, strpos($url, "?") + 1);
		if(!$queryString) return false;

		$pieces = explode("&", $queryString);
		if(sizeof($pieces) <= 0) return false;

		foreach($pieces as $kvPair) {
			if (strpos($kvPair, "=") === false) {
				return false;
			}
			list($key, $value) = explode("=", $kvPair);
			if($key == "v") {
				return trim($value);
			}
		}
		return false;
	}

	public static function imageFrame($path, $width, $height, $quality = 75, $contain = false) {

		$renderQuality = $quality;
		$restrictUpsize = true;
		$algorithmVersion = 7;

		$uploadPublicPath = config('cms.public_upload_directory', 'data/uploads');
		$uploadAbsolutePath = public_path($uploadPublicPath);

		if(!is_dir($uploadAbsolutePath) && !is_link($uploadAbsolutePath)) {
			mkdir($uploadAbsolutePath);
		}

		$cachePath = $uploadPublicPath . "/cache_" . md5($path) . "_{$width}x{$height}_q{$renderQuality}_up{$restrictUpsize}_c{$contain}_b{$algorithmVersion}.jpg";

		$originalPath = public_path($path);

		if(!$path || !is_file($originalPath)) {
			$cachePath .= ".404.jpg";
		}

		if(file_exists(public_path($cachePath))) {
			return $cachePath;
		}

		if(!is_file($originalPath)) {
			$img = Image::canvas($width, $height);
			$img->fill("#dddddd");

			$img->text("Image not found: {$originalPath}", ($width / 2), ($height / 2), function($font) {
				$font->align("center");
				$font->size(18);
			});

			$img->save(public_path($cachePath), 80);

			return $cachePath;
		}

		$img = Image::make($originalPath);


		if($width != null && $height != null) {
			if($contain) {

				$nw = ($width >= $height) ? $width : null;
				$nh = ($width < $height) ? $height : null;

				$img->resize($nw, $nh, function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				});

			} else {

				$img->fit($width, $height, function ($constraint) use ($restrictUpsize, $contain) {
					if($restrictUpsize) $constraint->upsize();
				});
			}
		}

		$img->save(public_path($cachePath), $renderQuality);

		return $cachePath;

	}

	public static function addURLParameter($in, $params) {

		if(strpos($in, "?") !== -1) {
			return $in . "?" . $params;
		}

		return $in . "&" . $params;

	}
}