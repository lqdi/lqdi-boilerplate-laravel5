# LQDI - Laravel 5 Project --REPLACE WITH PROJECT NAME--

## Laravel 5 Build Status
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


## EXPRESS INSTALL (Linux/OSX/Cygwin)

`curl -sS http://lqdi.net/deploy/boilerplate-laravel.sh | sh`

## Laravel Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## WARNING!

Please fill in basic project info in this documentation, for future develops to maintain this code.
Basic architecture overview and tricky choices made during develop are what's expected for this file.