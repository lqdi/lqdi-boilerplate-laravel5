<?php
return [

	'status_class' => [
		'failed' => 'danger',
		'error' => 'danger',
		'logged_out' => 'success'
	],

	'status_message' => [
		'failed' => 'Invalid username or password!',
		'logged_out' => 'You have been logged out!',
		'error' => 'An unexpected error has ocurred - error code: :errorCode'
	],
];